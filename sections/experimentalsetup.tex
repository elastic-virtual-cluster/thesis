\section{Experimental setup}
\label{sec:exp}

In order to evaluate the impact of virtualization and VM migration on performance, a set of experiments is carefully designed and performed.
The DAS-4 supercomputer that provides the infrastructure to run our system has a number of constraints and configuration specifics that need to be taken into account when designing these experiments.
The physical nodes that host the VMs each have 24 GB of RAM and 8 physical CPU cores with hyperthreading enabled - 16 virtual CPU cores in total.
While an Infiniband connection is available between the compute nodes on DAS-4, the module that allows for its use on VM level is disabled for easier maintenance.
This makes it necessary to use the slower 1Gbit Ethernet network both between VMs and also between physical hosts when performing experiments on bare metal for a more fair comparison.

The experiment design is discussed below, together with a more in-depth look into the various limitations and constraints in Section \ref{sec:limitations}.


%These hard limits need to be carefully taken into account when setting the memory and \emph{VCPU} parameters for the VMs in order to prevent overcommitting/contention.



\subsection{Latency and throughput measurements}

Before any experiments with a real-life parallel application (or a benchmark that runs a large number of processes) are performed, the effects of virtualization and VM migration on round-trip latency and throughput need to be measured on a smaller scale.
To this end, multiple latency and throughput tests were performed in the following setups:

Without virtualization (i.e. directly on compute nodes through SGE's reservation system):

\begin{itemize} \itemsep1pt \parskip0pt \parsep0pt
\item 2 processes on 1 cluster node
\item 2 processes on 2 cluster nodes (one on each)
\end{itemize}

With virtualization:

\begin{itemize} \itemsep1pt \parskip0pt \parsep0pt
\item 2 processes (one per VM) on 1 virtual cluster node
\item 2 processes (one per VM) on 2 virtual cluster nodes (one VM on each node)
\item migrating VMs from 1 to 2 nodes and back while the measurements are being done
\end{itemize}

By comparing the results from the tests when no VM migration is done (on both the physical and virtual clusters) we aim to isolate and measure the effects of virtualization on performance.
In the case when both processes are on the same physical node and no virtualization is used, the interprocess communication is done via shared memory.
%TODO double  check this - Kaveh said memory is used when both VMs are on same node=
We expect this to have an impact on both latency and throughput, as in the virtualization scenario the communication is done over TCP (via virtual network interfaces) when the 2 machines reside on the same node.
TCP is also used for transport in all other cases.
%(except when 2 processes are run directly on 1 cluster node without virtualization).

The following command is used in SGE jobfiles to force MPI to use TCP for transport:
\begin{lstlisting}[basicstyle = \small]
$MPI_RUN --mca btl tcp,self,sm --mca btl_tcp_if_include br0
-np $totcores -hostfile $HOSTFILE $APP $ARGS
\end{lstlisting}

where ``--mca btl tcp,self,sm --mca btl\_tcp\_if\_include br0'' is used to force the compute nodes to communicate with each other via TCP over the ``br0'' interface and use shared memory when interprocess communication is happening within the nodes.
Unless shared memory is enabled, the loopback ``lo'' interface is used for interprocess communication on nodes - this is known to be very slow.
The rest of the parameters are very commonly used when submitting jobs to SGE on DAS-4 and are, therefore, not discussed in detail.


A simple MPI latency benchmark was implemented in C and used to  make the measurements.
It measures round-trip latency by sending packets of size 1 byte as soon as possible, waiting for an acknowledgement and then recording the measured time difference and the associated timestamp.

A point-to-point bandwidth test from the OSU (Ohio State University) microbenchmark suite\footnote{http://mvapich.cse.ohio-state.edu/benchmarks/} was used to measure the effects of virtualization on bandwidth/throughput on bare metal (no virtualization) and in the virtual cluster.


\subsection{Parallel benchmarks} 

In order to estimate how a real-life application will be affected when run on our elastic virtual cluster, a set of benchmarks needs to be selected and carefully configured.
%TODO references for NPB
The NPB (NAS Parallel Benchmarks) suite is a popular choice for testing the performance of parallel supercomputers.
It contains several applications, written in multiple languages, that provide a wide choice of communication/memory access patterns.
The user can compile any of the benchmarks to use a specific number of processes and input size.
This allows for many degrees of freedom in the experiment design - using different classes of applications (e.g. ones that vary in their communication patterns), as well as different settings for each.

Running the benchmarks on a virtual cluster opens up a new dimension of parameters to experiment with.
The ones of particular interest are the number of VMs per host, the number of MPI processes running on a VM, the amount of memory and the number of virtual CPU-s (\emph{VCPU}-s) allocated to each VM.

On the highest level, there are parameters to the virtual cluster monitor that can be subject to experimentation, such as the time during execution at which a migration event (e.g. ``growing`` from $N$ to $2N$ physical host nodes) is triggered.

Ultimately, the goal is to examine a set of benchmarks of different classes, find the parameters that provide the optimal performance for each, compare this with the results from running the benchmarks directly on the physical nodes and identify the type of applications most positively affected by VM migration.

\emph{EP} (embarrassingly parallel), \emph{CG} (conjugate gradient) and \emph{MG} (multi-grid on a sequence of meshes) are good candidates to form this set - \emph{EP}, as the name suggests, being not very communication-intensive and \emph{CG} and \emph{MG} being the opposite.



\begin{table}[h]
	\centering
\begin{tabular}{|l|l|l|l|}
\hline
\textbf{benchmark} & \textbf{type}  & \textbf{num.} & \textbf{volume} \\ \hline
\textbf{EP}        & collective     & 5             & 12.5KB          \\ \hline
\textbf{}          & point-to-point & 0             & 0               \\ \hline
\textbf{CG}        & collective     & 1             & 0.03B           \\ \hline
\textbf{}          & point-to-point & 47104         & 2.24GB          \\ \hline
\textbf{MG}        & collective     & 100           & 126KB           \\ \hline
\textbf{}          & point-to-point & 11024         & 384MB           \\ \hline
\textbf{IS}        & collective     & 33            & 348MB           \\ \hline
                   & point-to-point & 15            & 0.06KB          \\ \hline
\end{tabular}
\label{tab:npbcomm}
\caption{Dynamic measurement for collective and point-to-point communications in EP, CG, MG and IS NPB benchmarks \cite{Faraj2002}}
\end{table}


%\begin{table}[h]
%\begin{tabular}{|l|l|l|l|}
%\hline
%\textbf{benchmark}           & \textbf{type}  & \textbf{num.} & \textbf{volume} \\ \hline
%\multirow{2}{*}{\textbf{EP}} & collective     & 5             & 12.5KB          \\ \cline{2-4} 
                             %& point-to-point & 0             & 0               \\ \hline
%\multirow{2}{*}{\textbf{CG}} & collective     & 1             & 0.03B           \\ \cline{2-4} 
                             %& point-to-point & 47104         & 2.24GB          \\ \hline
%\multirow{2}{*}{\textbf{MG}} & collective     & 100           & 126KB           \\ \cline{2-4} 
                             %& point-to-point & 11024         & 384MB           \\ \hline
%\multirow{2}{*}{\textbf{IS}} & collective     & 33            & 348MB           \\ \cline{2-4} 
                             %& point-to-point & 15            & 0.06KB          \\ \hline
%\end{tabular}
%\end{table}

Table \ref{tab:npbcomm} shows the number of collective and point-to-point communications and their associated volume for the EP, CG, MG and IS NPB benchmarks, compiled for 16 processes and problem size A \cite{Faraj2002}.
This can be used as a guideline to the amount of computation each benchmark is doing.


%\todo{mg.E fails, mg.D too short, hard to compare.. omit?}

Table \ref{tab:exp} shows a set of parameters used in our experiments.
Note that the results for only a small subset of these (focusing mostly on EP) are presented in Section \ref{sec:results}.

%TABLE?

%\begin{table}
	%\centering
	%\begin{tabular}{*{7}{>{\centering\arraybackslash}m{2em}}}
	%\toprule
%\begin{turn}{0} {\scriptsize bench.} \end{turn} &
%\begin{turn}{0} {\scriptsize input size} \end{turn} &
%\begin{turn}{0} {\scriptsize proc.} \end{turn} &
%\begin{turn}{0} {\scriptsize ph. nodes} \end{turn} &
%\begin{turn}{0} {\scriptsize VCPU} \end{turn} &
%\begin{turn}{0} {\scriptsize proc./VM} \end{turn} &
%\begin{turn}{0} {\scriptsize RAM} \end{turn}\\

	%\midrule
%\begin{tabular}{C}ep\\cg\\mg\\is\end{tabular} &
%\begin{tabular}{C}B\\C\\D\\E\end{tabular} &
%\begin{tabular}{C}8\\16\\32\\64\\128\\256\end{tabular} &
%\begin{tabular}{C}1\\2\\4\\8\\16\\32\\64\\128\end{tabular} &
%\begin{tabular}{C}1\\2\\16\end{tabular} &
%\begin{tabular}{C}1\\2\end{tabular} &
%\begin{tabular}{C}300\\400\end{tabular} \\
	%\bottomrule
	%\end{tabular}
	%\caption{parameters for experiments with parallel benchmarks}
	%\label{tab:exp}
%\end{table}



\begin{table}[h]
	\centering
\begin{tabular}{|l|l|}
\hline
\textbf{parameters} & \textbf{values}             \\ \hline
benchmark           & ep, cg, mg, is              \\ \hline
problem size        & B, C, D, E                  \\ \hline
processes           & 8. 16. 32. 64, 128, 256     \\ \hline
physical hosts      & 1, 2, 4, 8, 16, 32, 64 \\ \hline
VCPU                & 1, 2, 4, 8, 16              \\ \hline
RAM per VM          & 300 ... 3000                \\ \hline
processes per VM    & 1, 2, 4, 8, 16              \\ \hline
\end{tabular}
	\caption{parameters for experiments with parallel benchmarks}
	\label{tab:exp}
\end{table}

\subsection{Coarse-grained matrix multiplication}
\label{sec:expdgemm}

In addition to the benchmarks from NPB, matrix multiplication using the PDGEMM and PSGEMM routines from ScaLAPACK/PBLAS (\cite{Choia}) was performed to evaluate the effects of virtualization and live-migration of coarse-grained parallel applications.
%todo explain scalapack

PDGEMM is a level 3 matrix multiplication routine for block cyclic data distribution included in PBLAS, the parallel implementation of BLAS (Basic Linear Algebra Subprograms).
It is based on the DIMMA (\cite{Choi}) algorithm.
The algorithm requires $O(N^{3})$ flops and $O(N^{2})$ communications - it is computationally intensive but also performs a fair amount of communication.


A PDGEMM example program in Fortran provided by ScaLAPACK was adapted to use as a benchmark in the experiments.
It generates random matrices and allows the user to set the matrix size, block size and the dimensions of the process grid on a per-run basis.
This allows for a lot more freedom than in the NPB benchmarks, where the problem sizes (A, B \ldots E) are pre-defined.
The PSGEMM and PDGEMM benchmarks both perform the matrix multiplication as  $C := \alpha \times op(A) \times op(B) + \beta \times C$ (where $op(A)$ is defined as $A$ or its transpose) but differ in the datatype used - REAL being used in PSGEMM and DOUBLE PRECISION being used in PSGEMM.
PSGEMM is, therefore, expected to use less memory and less bandwidth than PDGEMM.
In order to make the application run for a longer time, the PSGEMM/PDGEMM routine was called multiple times in some of the experiments.



%\cite{BoKagstrom}




%- scalapack

%- parameters - matrix size, block size, number of processes

%- processes per VM 

%- PDGEMM and PSGEMM

%- make it run twice

%- why use NPB
%- why use benchmarks
%- flexibility
%- input size
%- which benchmarks, sizes, process
%- why ep and why cg/mg/etc
%- process configuration
%- vcpu settings
%- memory settings
%- migration - how it's done (also show commands)
%- aim - compare bare metal with virtualziation and virt + migration
%- what you get from the benchmkar
%- what you expect to see



\subsection{Limitations and constraints}
\label{sec:limitations}


%As the effects of memory contention are not of interest in this study, the distribution of VMs on hosts and the amount of memory allocated to each VM should be set to avoid any swapping on hosts or in VMs.


Configuring the parameters of both the parallel application and the virtual cluster is not a trivial task.
In case the amount of memory made available to a VM in the virtual cluster is exceeded by the cumulative amount of memory required by the application processes being run within it, performance is significantly degraded due to swapping on the VM level.
In the OpenNebula setup used for the experiments, swapping within VMs results in a very high  NFS activity on the head node of DAS-4, as the VM disk images are mounted over NFS.
This not only results in a failed experiment, but also negatively affects the work of other DAS-4 users and should certainly be avoided.
Additionally, if the cumulative amount of memory allocated to VMs on a host exceed the amount of physical memory available to that host, swapping on the host level results in degraded performance, as well.
While the memory contention in the latter scenario can be helped by migrating VMs away to new hosts, the effects of swapping on the host level are so detrimental that it makes starting the computation on a set of memory-contended hosts pointless.
Since swapping, both on the VM and host levels, is to be avoided, all experiments are planned to always satisfy the memory requirements of both VMs and hosts.


Since some performance penalty is expected during migration and this can possibly be compensated by the speedup achieved when the VMs are migrated to more hosts, the duration of the migration process (as in migrating a group of VMs to achieve a new distribution on hosts) should be short relative to the runtime of the parallel application.
In other words, the longer the runtime and the shorter the time it takes to get from $N/2$ to $N$ physical nodes, the less of an impact migration is expected to have.
Assuming physical resources are available and  can be reserved instantaneously, the time it takes to grow the virtual cluster and spread out VMs evenly is mostly determined by multiple factors - the number of VMs and the amount of memory allocated to each VM.
A third important factor that can greatly influence the migration time is the size of the writable working set - more memory-intensive applications can take much longer to reach the stop-and-copy phase in pre-copy migration.

Naturally, the fewer the VMs are, the fewer individual migration operations will take place.
This hints towards running several processes per VM in order to limit the total number of VMs.
However, having more processes per VM would (for most parallel applications) require more memory to be allocated per VM.
The preliminary tests of the experimental setup reveal that migrating VMs too quickly (as in starting too many individual migrations at the same time) results in many failed migrations - a VM enters the \emph{migr} state in OpenNebula and the copying to the destination host begins, but soon after migration fails and the VM continues to run on the source host.
Whether this is OpenNebula or Qemu/KVM related should be further investigated in the future.
In order to get around this problem, the application should wait for a fixed amount of time between individual VM migrations.
Another trend revealed in the preliminary tests is that when more memory is allocated per VMs, the waiting time should be increased as well in order to prevent migrations from failing.

The experiments are designed to always use at least 100\% of the available CPU resources on a physical host - typically going from 200\% to 100\% when migrating from $N/2$ to $N$ hosts.
Since the physical nodes on DAS-4 used in the experiment  each have 8 cores (16 with hyperthreading), parallel applications running at least 16 processes are required to adequately exploit migration to decrease the CPU contention but yet use at least 100\% in the new configuration.
This translates to having no fewer than 8 processes run per host, regardless of the number of VMs per host - the number of virtual CPUs (VCPUs) per VM can be set accordingly.
If hyperthreading is to be fully employed in the base (100\%) scenario, at least 32 processes, no fewer than 16 per host are needed.
As the processes in VMs are managed by the scheduler on the host, some overhead is expected due to CPU overcommitting if too many processes are being run per host \cite{Huber2011, Ranadive2008}.

%\todo{reference, mention our experiment, what \% of contention is ok}

The time at which migration occurs is expected to seriously affect performance in our system.
While this can be dependent on the application, its computation and communication patterns, assuming the workload is evenly distributed, both temporally and spatially, the sooner VMs are migrated to more nodes - the sooner computing is expected to speed-up if the application is CPU-bounded.
%\todo{say more about communication - this reasoning applies more to EP/coarse gr}



In summary, applications that run for longer and are migrated sooner (and in a shorter time) are expected to show better results.
This, however, applies mostly to embarrassingly parallel and coarse-grained parallel applications, for which migration done with the aim to lower the level of CPU contention is not likely to cause much communication overhead during and after migration.
The applications run in our virtual cluster have to be flexible enough to easily adapt to be run on 16, 32, 64 and more processes, and use such amount of memory per process as to allow for a placement of processes per VM and VMs per host, whereby swapping is avoided both on hosts and in VMs.
The total number of VMs and the number of processes per VM should be balanced to provide optimal performance benefits while keeping the duration/cost of migration low.
Parameters should be such that at least 100\% of the CPU resources are used but special care has to be taken not to cause overhead due to CPU overcommitting.



%- how long (at least) should bench run

%- wait after migration to prevent failure (maybe also sell this as a way to prevent multiple VMs from simultaneous rarp timeout)

%- how much RAM - on host and guest

%- what \% ot CPU contention

%- how many VMs - keep migration as quick as possible

%- when do we migrate

%- do not run things that write/read from disk - NFS

%- more processes per VM - better, exploit application specifics to place neighboring processes in one VM (future work)


